import os
import sys
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
os.environ['TF_GPU_THREAD_MODE'] = 'gpu_private'
import tensorflow as tf
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler

devices_id = 3
physical_devices = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(physical_devices[devices_id], True)
tf.config.set_visible_devices(physical_devices[devices_id], 'GPU')
np.random.seed(1234)
tf.random.set_seed(1234)

ds_name = sys.argv[1]


def main():
    df_processed = pd.read_csv("data/interim/{}_with_label_processed.csv".format(ds_name))
    df_processed = df_processed[df_processed['OS_MONTHS'] > 0]
    df_processed['OS_MONTHS'] = np.log(df_processed['OS_MONTHS'])
    scaler = StandardScaler()
    scaler.fit(df_processed[['OS_MONTHS']])
    df_processed['OS_MONTHS'] = scaler.transform(df_processed[['OS_MONTHS']])
    train, test = split_by_cancer_type(df_processed)
    train_ds = tf.data.Dataset.from_tensor_slices(dict(train))
    test_ds = tf.data.Dataset.from_tensor_slices(dict(test))
    write_example(train_ds, ds_name, 'train')
    write_example(test_ds, ds_name, 'test')
    print("Complete!")


def split_by_cancer_type(df):
    train_df = pd.DataFrame(columns=df.columns)
    test_df = pd.DataFrame(columns=df.columns)
    for i in df['cancerTypeId'].unique():
        tmp = df[df['cancerTypeId'] == i]
        for j in tmp['label'].unique():
            tmp_2 = tmp[tmp['label'] == j]
            msk = np.random.rand(len(tmp_2)) <= 0.8
            train_df = train_df.append(tmp_2[msk])
            test_df = test_df.append(tmp_2[~msk])
    train_df = train_df.astype({'OS_STATUS': 'int64', 'cancerTypeId': 'int64', 'label': 'int64'})
    test_df = test_df.astype({'OS_STATUS': 'int64', 'cancerTypeId': 'int64', 'label': 'int64'})
    return train_df, test_df


def _bytes_feature(value):
    """Returns a bytes_list from a string / byte."""
    if isinstance(value, type(tf.constant(0))):
        value = value.numpy()  # BytesList won't unpack a string from an EagerTensor.
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _float_feature(value):
    """Returns a float_list from a float / double."""
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def _int64_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def serialize_example(row, row_2):
    if row['cancerTypeId'] == row_2['cancerTypeId']:
        label = abs(row['label']-row_2['label'])+1
    else:
        label = 0
    feature = {
        'id_1': _bytes_feature(row['id']),
        'os_months_1': _float_feature(tf.cast(row['OS_MONTHS'], tf.float32)),
        'os_status_1': _int64_feature(row['OS_STATUS']),
        'cancer_type_1': _int64_feature(row['cancerTypeId']),
        'gene_1': _bytes_feature(row['gene']),
        'label_1': _int64_feature(row['label']),
        'id_2': _bytes_feature(row_2['id']),
        'os_months_2': _float_feature(tf.cast(row_2['OS_MONTHS'], tf.float32)),
        'os_status_2': _int64_feature(row_2['OS_STATUS']),
        'cancer_type_2': _int64_feature(row_2['cancerTypeId']),
        'gene_2': _bytes_feature(row_2['gene']),
        'label_2': _int64_feature(row_2['label']),
        'label':_int64_feature(label)
    }
    example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
    return example_proto.SerializeToString()


def write_example(dataset, name, subset):
    print("Start creating {} set...".format(subset))
    with tf.io.TFRecordWriter('data/processed/{}_{}_similarity.tfrecords'.format(name, subset)) as writer:
        nr = 0
        for row in dataset:
            nr += 1
            if (nr % 10000) == 0:
                print("On row: {}".format(nr))
            nr_2 = 0
            for row_2 in dataset:
                nr_2 += 1
                if (nr_2 % 10000) == 0:
                    print("On row_2: {}".format(nr_2))
                example = serialize_example(row, row_2)
                writer.write(example)
        print("Finish! Adding {} samples in {} set".format(nr*nr, subset))


if __name__ == "__main__":
    main()
