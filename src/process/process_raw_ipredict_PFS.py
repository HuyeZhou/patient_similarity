import pandas as pd
import numpy as np
import sys

which = sys.argv[1]

def main():
    df = pd.read_csv("data/raw/one_hot_Ipredict_data.csv")
    if which == "new":
        mutations = pd.read_csv("data/raw/one_hot_Ipredict_data_new.csv").iloc[:, 154:521]
    else:
        mutations = df.iloc[:, 154:512]
    df = df.iloc[:, [0,4,1,2]]
    df['gene_idx'] = mutations.astype(bool).apply(func,axis=1)
    df['gene'] = [','.join(map(str, i)) for i in df["gene_idx"]]
    df = df.drop(columns=["gene_idx"]).rename(columns={"Study.ID": "id", 
                                                       "OS_Censoring_Status": "cancerTypeId", 
                                                       "PFS_Months": "OS_MONTHS",
                                                       "PFS_Censoring_Status": "OS_STATUS"
                                                       })
    if which == "new":
        df.to_csv('data/interim/ipredict_new_processed.csv', header=True, index=False, mode='w')
        df_MTB = pd.read_csv("data/interim/MTB_new_processed.csv")
        df_MTB_IP = pd.concat([df_MTB, df])
        # df_MTB_IP.to_csv('data/interim/MTB_ipredict_PFS_new_processed.csv', header=True, index=False, mode='w')
    else:
        df.to_csv('data/interim/ipredict_PFS_processed.csv', header=True, index=False, mode='w')
        df_MTB = pd.read_csv("data/interim/MTB_processed.csv")
        df_MTB_IP = pd.concat([df_MTB, df])
        # df_MTB_IP.to_csv('data/interim/MTB_ipredict_processed.csv', header=True, index=False, mode='w')


def func(row):
    if which == "new":
        ints = np.array(range(0, 367))[row].tolist()
    else:
        ints = np.array(range(0, 358))[row].tolist()
    return([str(int) for int in ints])

if __name__ == "__main__":
    main()
