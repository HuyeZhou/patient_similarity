import pandas as pd
import numpy as np
import sys

which = sys.argv[1]

pd.options.mode.chained_assignment = None

df_os = pd.read_csv("data/raw/patients_with_OS.csv").drop_duplicates()
df_os['id'] = df_os['studyId'] + "." + df_os['patientId']
df_os = df_os[["id", "OS_MONTHS", "OS_STATUS", "cancerTypeId"]]
df_mutations = pd.read_csv("data/raw/patients_with_mutations.csv").drop_duplicates()
df_mutations['id'] = df_mutations['studyId'] + "." + df_mutations['patientId']
df_mutations = df_mutations[["id", "hugoGeneSymbol"]]
gene_list = df_mutations\
    .groupby("hugoGeneSymbol")\
    .count()\
    .rename(columns={"id": "count"})\
    .sort_values(['count'], ascending=False)\
    .reset_index(inplace=False)
gene_map = {}
n = 0
for i in gene_list.hugoGeneSymbol:
    gene_map[i] = n
    n += 1
gene_list['gene_idx'] = gene_list.hugoGeneSymbol.map(gene_map).apply(str)
df_mutations['gene_idx'] = df_mutations.hugoGeneSymbol.map(gene_map).apply(str)
mutations = df_mutations.groupby("id")["gene_idx"].unique().reset_index(inplace=False)
df_processed = pd.merge(df_os, mutations, on='id')
cancer_list = df_os[["id", "cancerTypeId"]]\
    .groupby("cancerTypeId")\
    .count()\
    .rename(columns={"id": "count"})\
    .sort_values(['count'], ascending=False)\
    .reset_index(inplace=False)
cancer_map = {}
n = 0
for i in cancer_list.cancerTypeId:
    cancer_map[i] = n
    n += 1
cancer_list['cancer_idx'] = cancer_list.cancerTypeId.map(cancer_map)
df_processed.OS_STATUS = df_processed.OS_STATUS.map({'1:DECEASED': 1, '0:LIVING': 0})
# df_processed["count"] = df_processed["gene_idx"].apply(len)
# df_processed = df_processed[df_processed['OS_MONTHS'] > 0]
# df_processed.drop(columns=["gene_idx"]).to_csv('data/interim/cbsp_processed_with_count.csv', header=True, index=False, mode='w')
df_processed['gene'] = [','.join(map(str, i)) for i in df_processed["gene_idx"]]
df_processed = df_processed.drop(columns=["gene_idx"])
df_processed["cancerTypeId"] = df_processed.cancerTypeId.map(cancer_map)

# cancer_list.to_csv('data/interim/cancer_list.csv', header=True, index=False, mode='w')
# gene_list.to_csv('data/interim/gene_list.csv', header=True, index=False, mode='w')
# df_processed.to_csv('data/interim/cbsp_processed.csv', header=True, index=False, mode='w')

MTB_df = pd.read_csv("data/raw/MTB_df.csv")
if which == "new":
    MTB_mutations = pd.read_csv("data/raw/one_hot_MTB_data_new.csv").iloc[:, 176:543]
else:
    MTB_mutations = pd.read_csv("data/raw/MTB_mutations.csv")
MTB_genes = list(MTB_mutations.columns)
MTB_cancer = list(MTB_df.columns)[15:33]
df_mutations_MTB = df_mutations[np.isin(df_mutations["hugoGeneSymbol"], MTB_genes)]
MTB_gene_map = {}
n = 0
for i in MTB_genes:
    MTB_gene_map[i] = n
    n += 1
df_mutations_MTB['gene_idx'] = df_mutations_MTB['hugoGeneSymbol'].map(MTB_gene_map).apply(str)
cbsp_mutations_MTB = df_mutations_MTB.groupby("id")["gene_idx"].unique().reset_index(inplace=False)
df_processed_MTB = pd.merge(df_os, cbsp_mutations_MTB, on='id')
df_processed_MTB.OS_STATUS = df_processed_MTB.OS_STATUS.map({'1:DECEASED': 1, '0:LIVING': 0})
# df_processed_MTB["count"] = df_processed_MTB["gene_idx"].apply(len)
# df_processed_MTB = df_processed_MTB[df_processed_MTB['OS_MONTHS'] > 0]
# df_processed_MTB.drop(columns=["gene_idx"]).to_csv('data/interim/cbsp_MTB_processed_with_count.csv', header=True, index=False, mode='w')
df_processed_MTB['gene'] = [','.join(map(str, i)) for i in df_processed_MTB["gene_idx"]]
df_processed_MTB = df_processed_MTB.drop(columns=["gene_idx"])
df_processed_MTB["cancerTypeId"] = df_processed_MTB.cancerTypeId.map(cancer_map)
if which == "new":
    df_processed_MTB.to_csv('data/interim/cbsp_MTB_new_processed.csv', header=True, index=False, mode='w')
else:
    df_processed_MTB.to_csv('data/interim/cbsp_MTB_processed.csv', header=True, index=False, mode='w')
