import pandas as pd
import numpy as np

def split_by_cancer_type(df):
    train_df = pd.DataFrame(columns=df.columns)
    test_df = pd.DataFrame(columns=df.columns)
    for i in df['cancerTypeId'].unique():
        tmp = df[df['cancerTypeId'] == i]
        for j in tmp['label'].unique():
            tmp_2 = tmp[tmp['label'] == j]
            msk = np.random.rand(len(tmp_2)) <= 0.8
            train_df = train_df.append(tmp_2[msk])
            test_df = test_df.append(tmp_2[~msk])
    train_df = train_df.astype({'cancerTypeId': 'int64', 'label': 'int64'})
    test_df = test_df.astype({'cancerTypeId': 'int64', 'label': 'int64'})
    return train_df, test_df

df_processed = pd.read_csv("data/interim/cbsp_MTB_with_label_processed.csv").iloc[:,[0,3,4,5]]
train, test = split_by_cancer_type(df_processed)
train = train.assign(key=1).merge(train.assign(key=1), on='key').drop('key',axis=1)
test = test.assign(key=1).merge(test.assign(key=1), on='key').drop('key',axis=1)
train['label'] = (1-abs(train.cancerTypeId_x - train.cancerTypeId_y))* (3-abs(train.label_x - train.label_y))
test['label'] = (1-abs(test.cancerTypeId_x - test.cancerTypeId_y))* (3-abs(test.label_x - test.label_y))
train.to_csv('data/processed/cbsp_MTB_with_label_train_ds.csv', header=True, index=False, mode='w')
test.to_csv('data/processed/cbsp_MTB_with_label_test_ds.csv', header=True, index=False, mode='w')