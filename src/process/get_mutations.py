from bravado.client import SwaggerClient
import pandas as pd

cbioportal = SwaggerClient.from_url('https://www.cbioportal.org/api/api-docs',
                                    config={"validate_requests": False, "validate_responses": False}
                                    )


def main():
    ds = pd.DataFrame(columns=["studyId", "patientId", "hugoGeneSymbol"])
    studies = cbioportal.Studies.getAllStudiesUsingGET().result()
    study_list = [i['studyId'] for i in studies]
    n = len(study_list)
    m = 0
    for i in study_list:
        try:
            m = m + 1
            print("start processing study {} ({}/{}), processed {} samples".format(i, m, n, len(ds)))
            mutations = get_mutation_data(i)
            mdf = pd.DataFrame.from_dict([dict(m._asdict(), **m._asdict()['gene']) for m in mutations])
            ds = pd.concat([ds, mdf[['patientId', 'studyId', 'hugoGeneSymbol']]])
            ds.to_csv('patients_with_mutations.csv', header=True, index=False, mode='w')

        except:
            pass
    ds.to_csv('data/raw/patients_with_mutations.csv', header=True, index=False, mode='w')
    print("Complete!")


def get_mutation_data(studyid):
    mutations = cbioportal.Mutations.getMutationsInMolecularProfileBySampleListIdUsingGET(
        molecularProfileId=studyid + '_mutations',
        sampleListId=studyid + '_all',
        projection='DETAILED',
    ).result()
    return mutations


if __name__ == "__main__":
    main()
