import sys
sys.path.append('../')
from utils import *

devices_id = int(sys.argv[12])
gpus = tf.config.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        tf.config.experimental.set_visible_devices(gpus[devices_id], 'GPU')
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
    except RuntimeError as e:
        print(e)

np.random.seed(1234)
tf.random.set_seed(1234)

bs = 16
ds_name = sys.argv[1]
subset_name = sys.argv[2]
training_ds_name = sys.argv[3]
input_dim = int(sys.argv[4])  # max = 21588
epochs = int(sys.argv[5])
lr = float(sys.argv[6])
l2 = float(sys.argv[7])
ds = int(sys.argv[8])
nf = int(sys.argv[9])
ld = int(sys.argv[10])
dr = float(sys.argv[11])

out_dir = 'output/models/VAE_REG_{}_{}_{}'.format(training_ds_name, input_dim, epochs)
logdir = 'output/models/VAE_REG_{}_{}_{}/logs'.format(training_ds_name, input_dim, epochs)
checkdir = 'output/models/VAE_REG_{}_{}_{}/checkpoints'.format(training_ds_name, input_dim, epochs)


def main():
    print("Create dataframe of dataset")
    test_ds = get_ds(ds_name, subset_name, input_dim, 0, bs, shuffle=False)
    test_df = get_df(ds_name, subset_name, training_ds_name, input_dim, True, 0)
    print("Load weight")
    vae, encoder, decoder = get_vae_model(input_dim, ld, l2, nf, dropout_rate=dr)
    vae.load_weights(checkdir + '/{}_{}_{}_{}_{}_{}'.format(lr, l2, ds, nf, ld, dr)).expect_partial()
    print("Start tesing of model")
    print("Perform kmean clustering")
    test_df = get_assignments(encoder, test_ds, test_df)
    test_df = get_z(encoder, test_ds, test_df)
    # print("Perform tsne")
    # tsne_test = tsne_visualization(encoder, 'test', test_ds)
    print("Save results")
    # tsne_test.savefig('output/figures/test_tsne_{}_{}_{}_{}_{}_{}_{}_{}_{}.png'.format(ds_name, input_dim, epochs, lr,
    #                                                                                    l2, ds, nf, ld, dr))
    test_df.to_csv("output/data/{}_df_{}_{}_{}_{}_{}_{}_{}_{}_{}_{}.csv".format(subset_name, ds_name, training_ds_name, input_dim, epochs, lr, l2, ds,
                                                                             nf, ld, dr), index=False)
    print("Complete!")


if __name__ == "__main__":
    main()
