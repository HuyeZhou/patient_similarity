import os
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
# os.environ['TF_GPU_THREAD_MODE'] = 'gpu_private'
import tensorflow as tf
import numpy as np
from tensorflow.keras.constraints import unit_norm
from tensorflow.keras.constraints import max_norm
from sklearn.metrics import r2_score
from tensorboard.plugins.hparams import api as hp
from sklearn.manifold import MDS
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
import pandas as pd
import matplotlib.pyplot as plt


def get_feature_description():
    feature_description = {
        'id': tf.io.FixedLenFeature((), tf.string),
        'os_months': tf.io.FixedLenFeature((), tf.float32),
        'os_status': tf.io.FixedLenFeature((), tf.int64),
        'cancer_type': tf.io.FixedLenFeature((), tf.int64),
        'gene': tf.io.FixedLenFeature((), tf.string)
    }
    return feature_description


def decode(serialized_example, n, m):
    example = tf.io.parse_single_example(serialized_example, get_feature_description())
    pid = example['id']
    os_months = example['os_months']
    os_months = tf.reshape(os_months, [-1])
    os_status = example['os_status']
    cancer_type = example['cancer_type']
    genes = example['gene']
    genes = tf.strings.split(genes, sep=',')
    genes = tf.strings.to_number(genes, tf.int32)
    genes = genes[genes < n]
    genes = tf.reshape(genes, [-1])
    mutations = tf.reduce_sum(tf.one_hot(genes, depth=n), axis=0)
    return mutations, pid, os_months, os_status, cancer_type, m


def filter_by_gene(mutations, pid, os_months, os_status, cancer_type, m):
    count = tf.reduce_sum(mutations)
    return tf.reshape(count > 0, [])


def filter_by_censoring(mutations, pid, os_months, os_status, cancer_type, m):
    return tf.reshape(os_status == 1, [])


def filter_by_cancer_type(mutations, pid, os_months, os_status, cancer_type, m):
    return tf.reshape(cancer_type == tf.cast(m, dtype=tf.int64), [])


def stack_training(mutations, pid, os_months, os_status, cancer_type, m):
    return (mutations, os_months), os_months


def stack_testing(mutations, pid, os_months, os_status, cancer_type, m):
    os_months = tf.reshape(os_months, [1, -1])
    return pid, os_months, os_status, cancer_type


def get_ds(dataset_name, subset, n, m, censoring, batch_size=16, shuffle=False):
    dataset = tf.data.TFRecordDataset("data/processed/{}_{}.tfrecords".format(dataset_name, subset))
    ds = dataset \
        .map(lambda x: decode(x, n, m), num_parallel_calls=tf.data.AUTOTUNE) \
        .filter(filter_by_gene)
    if censoring == 1:
        ds = ds.filter(filter_by_censoring)
    if m != 0:
        ds = ds.filter(filter_by_cancer_type)
    ds = ds \
        .map(stack_training, num_parallel_calls=tf.data.AUTOTUNE) \
        .cache()
    if shuffle:
        ds = ds.shuffle(40000, reshuffle_each_iteration=True)
    ds = ds.batch(batch_size).prefetch(tf.data.AUTOTUNE)
    return ds


def get_df(dataset_name, subset, training_ds_name, n, m, scale, censoring):
    dataset = tf.data.TFRecordDataset("data/processed/{}_{}.tfrecords".format(dataset_name, subset))
    ds = dataset \
        .map(lambda x: decode(x, n, m), num_parallel_calls=tf.data.AUTOTUNE) \
        .filter(filter_by_gene)
    if censoring == 1:
        ds = ds.filter(filter_by_censoring)
    ds = ds \
        .map(stack_testing, num_parallel_calls=tf.data.AUTOTUNE)
    df = [{'id': a.numpy().decode('utf-8'),
           'OS_MONTHS': b.numpy().item(),
           'OS_STATUS': c.numpy(),
           'cancer_type': d.numpy()} for a, b, c, d in ds]
    df = pd.DataFrame.from_records(df)
    if scale:
        cbsp_processed = pd.read_csv("data/interim/{}_processed.csv".format(training_ds_name))
        cbsp_processed = cbsp_processed[cbsp_processed['OS_MONTHS'] > 0]
        cbsp_processed['OS_MONTHS'] = np.log(cbsp_processed['OS_MONTHS'])
        scaler = StandardScaler()
        scaler.fit(cbsp_processed[['OS_MONTHS']])
        df['OS_MONTHS'] = np.exp(scaler.inverse_transform(df[['OS_MONTHS']]))
    return df


def sampling(args):
    mean, log_var = args
    batch = tf.keras.backend.shape(mean)[0]
    dim = tf.keras.backend.int_shape(mean)[1]
    epsilon = tf.keras.backend.random_normal(shape=(batch, dim))
    return mean + tf.exp(0.5 * log_var) * epsilon


def get_vae_model(original_dim, latent_dim, l2, nf, activation='tanh', dropout_rate=0.25):
    # build encoder
    regularizer = tf.keras.regularizers.l2(l2)
    inputs = tf.keras.Input(shape=(original_dim,))
    inputs_y = tf.keras.Input(shape=(1,))
    inputs_dropout = tf.keras.layers.Dropout(dropout_rate)(inputs)

    x = tf.keras.layers.Dense(8 * nf, activation=activation, kernel_regularizer=regularizer)(inputs_dropout)
    x = tf.keras.layers.Dense(4 * nf, activation=activation, kernel_regularizer=regularizer)(x)
    x = tf.keras.layers.Dense(2 * nf, activation=activation, kernel_regularizer=regularizer)(x)
    x = tf.keras.layers.Dense(nf, activation=activation, kernel_regularizer=regularizer)(x)

    z_mean = tf.keras.layers.Dense(latent_dim, name='z_mean')(x)
    z_log_var = tf.keras.layers.Dense(latent_dim, name='z_log_var')(x)
    y_mean = tf.keras.layers.Dense(1, name='y_mean')(x)
    y_log_var = tf.keras.layers.Dense(1, name='y_log_var')(x)
    z = tf.keras.layers.Lambda(sampling, output_shape=(latent_dim,))([z_mean, z_log_var])
    y = tf.keras.layers.Lambda(sampling, output_shape=(1,))([y_mean, y_log_var])
    pz_mean = tf.keras.layers.Dense(latent_dim, name='pz_mean', kernel_constraint=unit_norm())(y)
    pz_log_var = tf.keras.layers.Dense(1, name='pz_log_var', kernel_constraint=max_norm(0))(y)
    encoder = tf.keras.Model(inputs=[inputs, inputs_y],
                             outputs=[z_mean, z_log_var, z, y_mean, y_log_var, y],
                             name='encoder')

    # build decoder
    latent_inputs = tf.keras.Input(shape=(latent_dim,))
    x = tf.keras.layers.Dense(nf, activation=activation, kernel_regularizer=regularizer)(latent_inputs)
    x = tf.keras.layers.Dense(2 * nf, activation=activation, kernel_regularizer=regularizer)(x)
    x = tf.keras.layers.Dense(4 * nf, activation=activation, kernel_regularizer=regularizer)(x)
    x = tf.keras.layers.Dense(8 * nf, activation=activation, kernel_regularizer=regularizer)(x)
    output = tf.keras.layers.Dense(original_dim, activation='sigmoid')(x)
    decoder = tf.keras.Model(inputs=latent_inputs, outputs=output, name='decoder')
    outputs = decoder(encoder([inputs, inputs_y])[2])

    # build vae model
    vae = tf.keras.Model(inputs=[inputs, inputs_y], outputs=outputs, name='vae_mlp')

    # loss functions
    reconstruction_loss = tf.keras.losses.binary_crossentropy(inputs, outputs)
    kl_loss = 1 + z_log_var - pz_log_var \
              - tf.divide(tf.square(z_mean - pz_mean), tf.exp(pz_log_var)) \
              - tf.divide(tf.exp(z_log_var), tf.exp(pz_log_var))
    kl_loss = -0.5 * tf.reduce_sum(kl_loss, axis=-1)
    label_loss = tf.divide(0.5 * tf.square(y_mean - inputs_y), tf.exp(y_log_var)) + 0.5 * y_log_var
    vae_loss = tf.reduce_mean(reconstruction_loss + kl_loss + label_loss)
    vae.add_loss(vae_loss)
    vae.add_metric(reconstruction_loss, name='mse_loss', aggregation='mean')
    vae.add_metric(kl_loss, name='kl_loss', aggregation='mean')
    vae.add_metric(label_loss, name='label_loss', aggregation='mean')
    return vae, encoder, decoder


def train_test_model(hparams, vae, encoder, train_ds, test_ds, test_label, logdir, checkdir, epochs):
    l2 = hparams['l2']
    lr = hparams['lr']
    ds = hparams['ds']
    nf = hparams['nf']
    ld = hparams['ld']
    dr = hparams['dr']
    lr_schedule = tf.keras.optimizers.schedules.InverseTimeDecay(
        lr,
        decay_steps=2000,
        decay_rate=1,
        staircase=True)
    optimizer = tf.keras.optimizers.Adam(lr_schedule)
    vae.compile(optimizer=optimizer)
    vae.fit(
        train_ds,
        epochs=epochs,
        validation_data=test_ds,
        callbacks=[tf.keras.callbacks.TensorBoard(logdir + '/fit/{}_{}_{}_{}_{}_{}'.format(lr, l2, ds, nf, ld, dr)),
                   tf.keras.callbacks.ModelCheckpoint(
                       filepath=checkdir + '/{}_{}_{}_{}_{}_{}'.format(lr, l2, ds, nf, ld, dr),
                       monitor='val_label_loss', mode='min', save_weights_only=True, save_best_only=True),
                   tf.keras.callbacks.EarlyStopping(monitor='val_label_loss', min_delta=0, patience=epochs // 4,
                                                    mode='min')
                   ]
    )
    vae.load_weights(checkdir + '/{}_{}_{}_{}_{}_{}'.format(lr, l2, ds, nf, ld, dr))
    _, _, _, test_label_loss = vae.evaluate(test_ds)
    [_, _, _, test_y_mean, _, _] = encoder.predict(test_ds)

    ground_truth_y = np.array(test_label).reshape(-1, )
    test_pred = test_y_mean[:, 0]
    test_r2 = r2_score(ground_truth_y, test_pred)
    return test_label_loss, test_r2


def run(run_dir, hparams, vae, encoder, train_ds, test_ds, test_label, logdir, checkdir, epochs):
    with tf.summary.create_file_writer(run_dir).as_default():
        hp.hparams(hparams)
        loss, accuracy = \
            train_test_model(hparams, vae, encoder, train_ds, test_ds, test_label, logdir, checkdir, epochs)
        tf.summary.scalar('test_label_loss', loss, step=1)
        tf.summary.scalar('test_r_square', accuracy, step=1)


def get_assignments(encoder, ds, df):
    [z_mean, _, _, _, _, _] = encoder.predict(ds)
    kmean_results = KMeans(n_clusters=2, random_state=0).fit(z_mean)
    df['group'] = kmean_results.labels_
    return df


def get_z(encoder, ds, df):
    [z_mean, _, _, _, _, _] = encoder.predict(ds)
    df = pd.concat([df, pd.DataFrame(z_mean)], axis=1)
    return df


def tsne_visualization(encoder, subset, ds):
    if subset == "test":
        [x, _, _, _, _, _] = encoder.predict(ds)
    else:
        x = np.concatenate([x for (x, _), _ in ds], axis=0)
    label = np.concatenate([y for _, y in ds], axis=0)
    tsne = MDS(n_components=2, random_state=0)
    x_2d = tsne.fit_transform(x)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(x_2d[:, 0], x_2d[:, 1], c=label)
    plt.title('TSNE visualization for latent space of {} set'.format(subset))
    ax.axis('equal')
    return fig
